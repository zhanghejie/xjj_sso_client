<%@ page language="java" import="java.util.*,com.xjj.sso.client.user.SSOReceipt,com.xjj.sso.client.SSOConstants" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
SSOReceipt receipt = (SSOReceipt)session.getAttribute(SSOConstants.SSO_CLIENT_RECEIPT);

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  </head>
  
  <body>
    欢迎您!
    <%=receipt.getUserName()%>
    <br>
    <a href="<%=SSOConstants.SSO_SERVER_SSOLOGOUTURL%>?service=http://localhost:8080/xjj_sso_client/sso/login.jsp">退出</a>
  </body>
</html>
