package com.xjj.sso.client.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SSOFilePropertyConfigLoader implements SSOConfigLoader {
 
	private String fileName1 = "/config/xjj_sso_client.properties";
	private String fileName2 = "/xjj_sso_client.properties";
	private String fileName = "/config/xjj_sso_client.properties";
	
	public SSOFilePropertyConfigLoader(){
    }
	public SSOFilePropertyConfigLoader(String fileName1,String fileName2){
    	this.fileName1 = fileName1;
    	this.fileName2 = fileName2;
   	}

	public Properties load() throws Exception{
		Properties properties = new Properties();
		InputStream is = null;
    	try {
    		is = getClass().getResourceAsStream(fileName1);
    		if(null == is)
    		{
    			is = getClass().getResourceAsStream(fileName2);
    			fileName = fileName2;
    		}
    		properties.load(is);
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally{
    		if(null != is)
    		{
    			is.close();
    		}
    	}
    	return properties;
    }
	
	public void save(Properties properties) throws Exception {
		try {
			File propFile = new File(this.fileName);
			properties.store(new FileOutputStream(propFile), "");
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Can't Save Properties to File:" + fileName);
		}
	}
}