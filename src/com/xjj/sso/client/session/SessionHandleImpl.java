package com.xjj.sso.client.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.xjj.sso.client.SSOConstants;
import com.xjj.sso.client.filter.SSOClientFilter;
import com.xjj.sso.client.user.SSOReceipt;

public class SessionHandleImpl implements SessionHandle{

	/**
	 * 登陆成功功以后，调用该方法
	 */
	public void handle(HttpServletRequest request,SSOReceipt receipt){
		
		HttpSession session = request.getSession();
		session.setAttribute(SSOConstants.SSO_CLIENT_RECEIPT, receipt);
		
	}
	
	
	/**
	 * sso退出被踢出以后，调用该方法,各业务系统自行处理
	 */
	public void ssoLogout(String ticketId,HttpSession session)
	{
		if(null ==session)
		{
			return ;
		}

		//TODO 这里可以得到哪个用户被踢出了，也可以得到业务系统自已放在session里的东西
		
		try {
			//干掉session
			session.invalidate();
		} catch (Exception e) {
			
		}
		//若把session干掉后,一定要把缓存也清除掉。
		SSOClientFilter.SESSION_TICKET_CACHE.remove(session.getId());
		SSOClientFilter.TICKET_SESSION_CACHE.remove(ticketId);
		
	}
}
