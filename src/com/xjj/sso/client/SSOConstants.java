package com.xjj.sso.client;

import com.xjj.sso.client.util.SSOConfiguration;

public class SSOConstants {
	
	public static final String HTTP_PRE = "http://";
	public static final String HTTPS_PRE = "https://";
	
	public static final String SSO_SERVER_URL = SSOConfiguration.get("sso.server.url");
	public static final String SSO_SERVER_SIGNIN = SSO_SERVER_URL+SSOConfiguration.get("sso.server.signin");
	public static final String SSO_SERVER_AUTHENTICATION = SSO_SERVER_URL+SSOConfiguration.get("sso.server.authentication");
	public static final String SSO_SERVER_VALIDATETICKET = SSO_SERVER_URL+SSOConfiguration.get("sso.server.validateTicket");
	public static final String SSO_SERVER_SSOLOGOUTURL = SSO_SERVER_URL+SSOConfiguration.get("sso.server.ssoLogoutUrl");
	//public static final String SSO_SERVER_SSOLOGOUTHTTP = SSO_SERVER_URL+SSOConfiguration.get("sso.server.ssoLogoutHttp");
	public static final String SSO_SERVER_ERROR = SSOConfiguration.get("sso.server.error");
	
	
	public static final String SSO_CLIENT_RECEIPT = SSOConfiguration.get("sso.client.receipt");
	public static final String SSO_CLIENT_SERVERNAME = SSOConfiguration.get("sso.client.serverName");
	public static final String SSO_CLIENT_SESSIONHANDLE =  SSOConfiguration.get("sso.client.sessionHandle");
	public static final String SSO_CLIENT_SSOLOGONURL =  SSOConfiguration.get("sso.client.ssoLogonUrl");
	public static final String SSO_CLIENT_SSOLOGONBACK =  SSOConfiguration.get("sso.client.ssoLogonBack");
	
	// 项目代码
	public static final String SSO_CLIENT_PROJECTCODE =  SSOConfiguration.get("sso.client.projectCode");
			
	public static final String SSO_CLIENT_LOGINURL =  SSOConfiguration.get("sso.client.loginUrl");
	public static final String SSO_CLIENT_LOGIN_ISREDIRECT =  SSOConfiguration.get("sso.client.login.isredirect");
	public static String SSO_NOTIFICATION_TYPE=SSOConfiguration.get("sso.notification.type");
}